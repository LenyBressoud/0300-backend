import express from 'express';
const router = express.Router()

import model from '../models/chienModel.js';

router.use(express.json());
router.use(express.urlencoded({ extended: true }))

router.get('/', async (req, res) => {
    const cours = await model.find()
    res.send(cours)
})

router.post('/', async (req, res) => {
    cours = model.create({
        "identificationAmicus": req.body.amicus,
        "proprietaire_id": req.body.proprietaire_id,
        "dateNaissance": req.body.date,
        "male": req.body.male,
        "race": req.body.race,
        "nom": req.body.nom
    })
})

router.put('/', async (req, res) => {
    cours = model.findOneAndUpdate({ "_id": req.body._id }, { "$set": {
        "identificationAmicus": req.body.amicus,
        "proprietaire_id": req.body.proprietaire_id,
        "dateNaissance": req.body.date,
        "male": req.body.male,
        "race": req.body.race,
        "nom": req.body.nom
    }})
})

router.delete('/', async (req, res) => {
    model.deleteOne({ "_id": req.body._id });
})

export default router