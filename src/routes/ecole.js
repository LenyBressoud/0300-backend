import express from 'express';
const router = express.Router()

import model from '../models/ecoleModel.js';

router.use(express.json());
router.use(express.urlencoded({ extended: true }))

router.get('/', async (req, res) => {
    const chiens = await model.find()
    res.send(chiens)
})

router.post('/', async (req, res) => {
    cours = new model({
        "addresse": req.body.addresse,
        "localite": req.body.localite,
        "mailEcoles": req.body.mails,
        "telephoneEcoles": req.body.telephones
    })
})

router.put('/', async (req, res) => {
    cours = model.findOneAndUpdate({ "_id": req.body._id }, { "$set": {
        "addresse": req.body.addresse,
        "localite": req.body.localite,
        "mailEcoles": req.body.mails,
        "telephoneEcoles": req.body.telephones
    }})
})

router.delete('/', async (req, res) => {
    model.deleteOne({ "_id": req.body._id });
})

export default router