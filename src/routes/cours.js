import express from 'express';
const router = express.Router()

import model from '../models/coursModel.js';

router.use(express.json());
router.use(express.urlencoded({ extended: true }))

router.get('/', async (req, res) => {
    const chiens = await model.aggregate([
        { $project: { tarif: 1, niveau: 1, type: 1 } }
    ]).exec()
    res.send(chiens)
})

router.post('/', async (req, res) => {
    cours = new model({
        "tarif": req.body.tarif,
        "niveau": req.body.niveau,
        "type": req.body.type
    })
})

router.put('/', async (req, res) => {
    cours = model.findOneAndUpdate({ "_id": req.body._id }, { "$set": {
        "niveau": req.body.niveau,
        "tarif": req.body.tarif,
        "type": req.body.type
    }})
})

router.delete('/', async (req, res) => {
    model.deleteOne({ "_id": req.body._id });
})

export default router