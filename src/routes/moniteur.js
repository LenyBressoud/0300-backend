import express from 'express';
const router = express.Router()

import model from '../models/moniteurModel.js';

router.use(express.json());
router.use(express.urlencoded({ extended: true }))

router.get('/', async (req, res) => {
    const chiens = await model.find()
    res.send(chiens)
})

router.post('/', async (req, res) => {
    cours = model.create(req.body)
})

router.put('/', async (req, res) => {
    cours = model.findOneAndUpdate({ "_id": req.body._id }, { "$set": req.body})

    res.send(JSON.stringify(cours))
})

router.delete('/', async (req, res) => {
    model.deleteOne({ "_id": req.body._id });
})

export default router