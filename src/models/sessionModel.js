import { Schema as _Schema, model as _model, ObjectId as _ObjectId } from 'mongoose';

const Schema = _Schema;
const model = _model;
const ObjectId = _ObjectId

const ecoleSchema = new Schema({
    "coursId": ObjectId,
    "date": Date,
    "dureeM": Number,
    "moniteurId": ObjectId,
    "places": Number
}, { collection: "ecole" })

export default model("ecole", ecoleSchema);