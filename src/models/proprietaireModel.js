import { Schema as _Schema, model as _model, ObjectId as _ObjectId } from 'mongoose';
import { localite, mail, telephone } from '../types';

const Schema = _Schema;
const model = _model;
const ObjectId = _ObjectId

const proprietaireSchema = new Schema({
    "addresse": String,
    "certificatDelivre": Boolean,
    "dateNaissance": Date,
    "nom": String,
    "prenom": String,
    "origine": String,
    "localite": localite,
    "mailProprietaires": [
        mail
    ],
    "telephoneProprietaires": [
        telephone
    ]
}, { collection: "proprietaire" })

export default model("proprietaire", proprietaireSchema);