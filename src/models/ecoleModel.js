import { Schema as _Schema, model as _model, ObjectId as _ObjectId } from 'mongoose';
import { localite, mail, telephone } from '../types';

const Schema = _Schema;
const model = _model;
const ObjectId = _ObjectId

const ecoleSchema = new Schema({
    "addresse": String,
    "localite": localite,
    "mailEcoles": [
        mail
    ],
    "telephoneEcoles": [
        telephone
    ]
}, { collection: "ecole" })

export default model("ecole", ecoleSchema);