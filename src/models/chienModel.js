import { Schema as _Schema, model as _model, ObjectId as _ObjectId } from 'mongoose';

const Schema = _Schema;
const model = _model;
const ObjectId = _ObjectId

const chienSchema = new Schema({
    "identificationAmicus": String,
    "proprietaire_id": ObjectId,
    "dateNaissance": Date,
    "male": Boolean,
    "nom": String,
    "race": {
        "nom": String
    }
}, { collection: "chien" })

export default model("chien", chienSchema);