import { Schema as _Schema, model as _model, ObjectId as _ObjectId } from 'mongoose';

const Schema = _Schema;
const model = _model;
const ObjectId = _ObjectId

const coursSchema = new Schema({
    "tarif": Number,
    "niveau": String,
    "type": String,
}, { collection: "cours" })

export default model("cours", coursSchema);