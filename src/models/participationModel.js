import { Schema as _Schema, model as _model, ObjectId as _ObjectId } from 'mongoose';

const Schema = _Schema;
const model = _model;
const ObjectId = _ObjectId

const participationSchema = new Schema({
    "avecProprietaire": Boolean,
    "chienId": ObjectId,
    "sessionId": ObjectId
}, { collection: "participation" })

export default model("participation", participationSchema);