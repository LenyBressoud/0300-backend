import { Schema as _Schema, model as _model, ObjectId as _ObjectId } from 'mongoose';
import { localite, mail, telephone } from '../types';

const Schema = _Schema;
const model = _model;
const ObjectId = _ObjectId

const moniteurSchema = new Schema({
    "addresse": String,
    "civilite": String,
    "dateNaissance": Date,
    "nom": String,
    "prenom": String,
    "localite": localite,
    "ecoleId": ObjectId,
    "mailMoniteurs": [
        mail
    ],
    "telephoneMoniteurs": [
        telephone
    ]
}, { collection: "moniteur" })

export default model("moniteur", moniteurSchema);