import { connect } from 'mongoose'
import express from 'express'
const app = express()
const port = 3000

import cours from './routes/cours.js'
 
main().catch(err => console.log(err))

async function main() {
  await connect('mongodb://127.0.0.1:27017/coursCanin')
  console.log("Connected to the database")

  app.get('/', async (req, res) => {
    res.send("Welcome the the REST API")
  })

  app.use('/cours', cours)

  app.listen(port)
  console.log(`App listening on port ${port}`)
}