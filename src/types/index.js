
export const mail = {
    "mail": String,
    "type": {"type": String}
}

export const telephone = {
    "numero": String,
    "type": {"type": String}
}

export const localite = {
    "localite": String,
    "npa": Number
}