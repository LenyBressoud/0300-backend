# 0300 Backend

## Objectifs

- Créer une API Rest pour la base de données cours canin

## Ressources

* [Documentation Git](https://git-scm.com/book/en/v2)
* [Documentation Node.js](https://nodejs.org/docs/latest/api/)
* [Documentation Mongoose](https://mongoosejs.com/docs/guide.html)
* [Documentation Express JS](https://expressjs.com/)

## Matériel
- Poste de travail
- Installation de MongoDB sur Docker
- MongoDB Compass
- [Node.js](https://nodejs.org/en/download)

## Consignes

* Complétez le présent document avec les commandes utilisées, en particuliter les sections **TODO**
* Votre <visa> est toujours composé des 4 premières lettres de votre prénom et des 4 premières lettres de votre nom

## Introduction

Le but de cet exercice est de réaliser une [API REST](https://www.redhat.com/fr/topics/api/what-is-a-rest-api) permettant un [CRUD](https://fr.wikipedia.org/wiki/CRUD) de la base de données "cours canin" réalisée durant les exercices précdents.

### Créer une merge request

Une fois terminé, créez une merge request
```
<visa>: Travail terminé
```

